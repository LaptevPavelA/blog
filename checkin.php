<?php
	error_reporting(E_ALL);
	ini_set('error_reporting', E_ALL);
	ini_set('displey_errors', 1);
	ini_set('displey_startup_errors', 1);
	require_once 'app/header.php';
  require_once $_SERVER['DOCUMENT_ROOT']. "/"."app/footer.php";	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
  <link href="/public/css/bootstrap.min.css" rel="stylesheet">
  <link href="/style.css" rel="stylesheet">
</head>
<script>
	function reg_button (){
		var item = document.getElementsByName("done")[0];
			if (info_login.innerHTML === 'логин корректный' && info_email.innerHTML ==='email свободен' && info_password.innerHTML === 'пароль корректный' && info_password1.innerHTML === 'пароли совпадают'){
					item.disabled = false;
			} 
				else {
					item.disabled = true;
				}
	}

    function login_control(login) {
    		if(/^[a-zA-Z1-9]+$/.test(login.value) === false){
   			 	document.querySelector('#info_login').innerHTML = 'в логине должны быть только латинские буквы и цифры';
          document.getElementById('info_login').style.color = "red"; 
   			 		//document.querySelector('#info_login').innerHTML = 'логин некорректный';	
   			 			reg_button();
   			 				return false;
   			}
				if(login.value.length < 4 || login.value.length > 20){
    				document.querySelector('#info_login').innerHTML ='в логине должен быть от 4 до 20 символов';
            document.getElementById('info_login').style.color = "red";
    				 	//document.querySelector('#info_login').innerHTML = 'логин некорректный'; 
    				 		reg_button();
    				 			return false;
    				}
							if(parseInt(login.value.substr(0, 1))){
   								document.querySelector('#info_login').innerHTML = 'логин должен начинаться с буквы';
                  document.getElementById('info_login').style.color = "red"; 
   									reg_button();
   										return false;
   							}
								document.querySelector('#info_login').innerHTML = 'логин корректный';
                document.getElementById('info_login').style.color = "green";
									reg_button();
										return true;
	}
 
 	function password_control(password) {
 		var password1 = document.getElementsByName('password1')[0];
 			if(/^[а-яА-ЯёЁ\w.,\?!]+$/.test(password.value) === false){
        document.querySelector('#info_password').innerHTML = 'пароль должен содержать только буквы, цифры и символы ".", ",", "?", "!"';
        document.getElementById('info_password').style.color = "red"; 
    				//document.querySelector('#info_password').innerHTML = 'пароль некорректный';
    					password1.value = '';
    						document.querySelector('#info_password1').innerHTML = '';
    							reg_button();
    								return false;}
    			if (password.value.length < 6 || password.value.length > 20){
              document.querySelector('#info_password').innerHTML = 'в пароле должен быть от 6 до 20 символов';
              document.getElementById('info_password').style.color = "red";
    					//document.querySelector('#info_password').innerHTML = 'пароль некорректный';
    						password1.value = '';
    							document.querySelector('#info_password1').innerHTML = '';
 								reg_button();
 									return false;}
   										document.querySelector('#info_password').innerHTML = 'пароль корректный';
                      document.getElementById('info_password').style.color = "green";
    												password1.value = '';
    													//document.querySelector('#info_password1').innerHTML = 'пароли не совпадают';
    														reg_button();
    }

    function password1_control(password1) {
    	var password = document.getElementsByName('password')[0]; 
  			if (!(password.value === password1.value)){
    			//alert ("Пароли не совпадают")
    				document.querySelector('#info_password1').innerHTML = 'пароли не совпадают';
            document.getElementById('info_password1').style.color = "red";
    					reg_button();
    						return false;
    		}
    			document.querySelector('#info_password1').innerHTML = 'пароли совпадают';
          document.getElementById('info_password1').style.color = "green";
    				reg_button();
    }		
		

function ajax (){
	var email = document.getElementsByName('email')[0].value;
  if (email === ""){
    document.querySelector ('#info_email').innerHTML = "";
    return  
  }
		fetch('ajax/ajax.php', {  
    		method: 'post',  
    		headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},  
    		body: JSON.stringify({email:email})
 		}) 
  			.then(function(response){  
     				if (response.status !== 200) {  
        				console.log('Looks like there was a problem. Status Code: ' +  
          					response.status);  
        						return;  
     				 }
     					response.text().then(function(data){ 
     							document.querySelector ('#info_email').innerHTML = data;
                  data === "email занят"?document.getElementById('info_email').style.color = "red":document.getElementById('info_email').style.color = "green"; 
     							reg_button();
     							});  
    		})  
  				.catch(function(err) {  
    				console.log('Fetch Error :-S', err);  
  				});
} 

  </script>
<body>
<div class = "well" style="width: 300px; margin-left: 290px">
	<div class = "form-group">
			<h3>Регистрация</h3>
			<div class="row">
        			<br/>
      			</div>
      			<form action="app/registering.php" method="POST">
		<input type = "email" name="email" value="" class="form-control" placeholder="Введите свой email" onchange ="ajax()" required="">
		<div id= "info_email"></div>			
	</br>
		<input type = "text" name="login" value="" class="form-control"placeholder="Введите логин" oninput="login_control(this)" required="">
		<div id= "info_login"></div>	
	</br>
		<input type = "password" name="password" value="" class="form-control"placeholder="Введите пароль" oninput="password_control(this)"required="">
		<div id= "info_password"></div>
	</br>
		<input type = "password" name="password1" value="" class="form-control"placeholder="Введите пароль повторно"oninput="password1_control(this)" required="">
		<div id= "info_password1"></div>
	</br>
		<button disabled type="submit" name="done" class="btn btn-success" onclick="">Зарегестрироваться</button>
	</br>
				</form>
        </div>
		<div id= "information"></div>
	</div>
</div>
</body>
</html>

