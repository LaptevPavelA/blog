<?php
session_start();
	if (isset($_GET['status'])){
		session_destroy();
		header('Location:/admin.php/');
	}
	error_reporting(E_ALL);
	ini_set('error_reporting', E_ALL);
	ini_set('displey_errors', 1);
	ini_set('displey_startup_errors', 1);
	require_once $_SERVER['DOCUMENT_ROOT']. "/"."app/include/database.php";
	global $link;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="stylesheet" type="text/css" href="style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<meta charset="UTF-8">
	<title>Document</title>
	<link href="/style.css" rel="stylesheet">
	<style type="text/css">
	 table {
font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
font-size: 14px;
border-collapse: collapse;
text-align: center;
}

th, td {
border-style: solid;
border-width: 0 1px 1px 0;
border-color: black;
}

th:first-child, td:first-child {
text-align: left;
}
</style>

</head>
<body>
	<p id = "out"></p>
	<!-- ВСПЛЫВАЮЩЕЕ ОКНО-->
<div id="modal_form"><!-- Сaмo oкнo --> 
		<div>Для подтверждения удаления учетной записи введите ваш пароль</div>
	</br>
      <span id="modal_close">X</span> <!-- Кнoпкa зaкрыть --> 
      <input type = "password" id="delete_user_password_control_input" value="" class="form-control"placeholder="Введите пароль" required="">
      <button id = "delete_user_password_control_button">OK</button>
</div>
<div id="overlay"></div><!-- Пoдлoжкa -->


<div class = "well">
	<div class = "form-group">
		<?php if (!isset ($_SESSION['admin'])):  ?>
		<form action="/admin/adminka.php" method="POST">
			<div class="col-md-9">
		<input type = "email" name="email" value="" class="form-control"placeholder="Введите свой email" required="">
	</br>
		<input type = "password" name="password" value="" class="form-control"placeholder="Введите пароль" required="">
	</br>
		<button type="submit" class="btn btn-success">Вход</button>		
		</form>

		<!--ПРОВЕРКА УРОВНЯ ДОСТУПА-->
			<?php elseif (isset ($_SESSION['admin']) && $_SESSION['admin'] === 'ok'): ?>
			<div id = email_admin><?=$_SESSION ['email']?></div>

			<table id = admin_table bordercolor="black">
				<tr>
				<th>id</th> 
				<th>email</th> 
				<th>status</th> 
				<th>code</th> 
				<th>password</th> 
				<th>login</th>
				<th colspan="2">Действия</th> 
				</tr>
				<?php
				$link = new LinkDB;
					$result=$link->getTableByDB('subscribers');
				?>
				 <?php for ($i=0; $i<count ($result); $i++):?>		 
				 	<form action="/admin/adminka.php" method="POST">
    <tr>
        <td class = "<?=$result[$i]['id']?>"><?=$result[$i]['id']?></td>
        <td class = "<?=$result[$i]['id']?>"><?=$result[$i]['email']?></td>
        <td>

        	<!--ПРОВЕРКА И ВЫВОД СТАТУСОВ ЮЗЕРОВ И АДМИНОВ-->
            <?php if($result[$i]['status'] === '1' && !($_SESSION['admin_id'] === $result[$i]['id'])):?>
            <select required name="status">
                <option class = "<?=$result[$i]['id']?>" disabled value="1">Admin-доступ</option>
                <option value="1+<?=$result[$i]['id']?>">Admin-доступ
                <option value="0+<?=$result[$i]['id']?>">User-доступ
            </select>	
                	<?php elseif ($result[$i]['status'] === '1' && $_SESSION['admin_id'] === $result[$i]['id']):?>
                		<div class = "<?=$result[$i]['id']?>">Admin-доступ</div>
			<?php else:?>
			<select  required name="status">
				<option class = "<?=$result[$i]['id']?>" disabled value="0">User-доступ</option>
            	<option value="0+<?=$result[$i]['id']?>">User-доступ
            	<option value="1+<?=$result[$i]['id']?>">Admin-доступ
            <?php endif;?>
            </select>
        </td>	
        <td class = "<?=$result[$i]['id']?>"><?=$result[$i]['code']?></td>
        <td class = "<?=$result[$i]['id']?>"><?=$result[$i]['password']?></td>
        <td class = "<?=$result[$i]['id']?>"><?=$result[$i]['login']?></td>
        <?php if ($result[$i]['email'] == $_SESSION['email']): ?>
        		<td class = "<?=$result[$i]['id']?>"><button type="submit" disabled="disabled">Сохранить</button></td>
        			<?php else:?>
        				<td class = "<?=$result[$i]['id']?>"><button type="submit">Сохранить</button></td>
        	<?php endif;?>
        </form>	
        <?php if ($result[$i]['email'] == $_SESSION['email']): ?>
       		<td><button disabled="disabled"  id = "<?=$result['id']?>" value = "<?=$result['id']?>" onclick= "delete_user(this)">Удалить аккаунт</button></td>
       			<?php else:?>
       				<td><button id = "<?=$result[$i]['id']?>" value = "<?=$result[$i]['id']?>" onclick= "delete_user(this)">Удалить аккаунт</button></td>
       					<?php endif;?>
    </tr>

    <?php endfor;?>

    <!-- ВЫВОД ОТВЕТА ОБ ИЗМЕНЕНИИ СТАТУСА-->
    <button><a href=/admin.php/?status=exit">Выход</a></button>
    <?php if (isset($_GET['result'])):?>
    <div class ='' style="background-color: red">Статус юзера №<?=$_GET['result']?> изменен</div>
    <?php endif;?>

			<?php else: ?>
			<div> Доступ запрещен</div>	
			<?php endif;  ?>
		</form>
	</div>
</div>
       			<script src="../script.js"></script>
       			
</body>
</html>